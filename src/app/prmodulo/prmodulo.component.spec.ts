import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrmoduloComponent } from './prmodulo.component';

describe('PrmoduloComponent', () => {
  let component: PrmoduloComponent;
  let fixture: ComponentFixture<PrmoduloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrmoduloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrmoduloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
