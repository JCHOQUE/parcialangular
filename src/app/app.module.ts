import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrmoduloComponent } from './prmodulo/prmodulo.component';
import { SgmoduloComponent } from './sgmodulo/sgmodulo.component';

@NgModule({
  declarations: [
    AppComponent,
    PrmoduloComponent,
    SgmoduloComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
